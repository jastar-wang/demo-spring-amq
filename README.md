# demo-spring-amq

#### 项目介绍
本项目用于演示 `Spring` 整合 `ActiveMQ` 的相关配置及使用。

#### 软件架构
本项目为 `SpringMVC`的 `Javaweb` 应用，使用 `Maven` 进行构建管理。
- jdk：1.7
- spring：4.1.8.RELEASE
- activemq-spring：5.14.5

#### 安装教程
> 你的本地必须得装了ActiveMQ才可以测试哟

1. 点击 [这里](https://gitee.com/jastar-wang/demo-spring-amq.git) 进行下载或 `clone`
2. 将本项目导入到 `IDE` 工具中
3. 将本项目部署到 `Tomcat` 下并启动
4. 打开浏览器访问：[http://localhost:8080/demo-spring-amq/](http://localhost:8080/demo-spring-amq/)
5. 按照界面操作即可
	
#### 使用说明
- 如您在使用过程中发现任何问题，先嫑着急揍我，请使用 [Isuse](https://gitee.com/jastar-wang/demo-spring-amq/issues)
- 本项目采用 [MIT](https://mit-license.org/) 开源协议，您在保留作者版权的情况下可以自由分享和使用该代码

#### 参与贡献
1. `Fork` 本项目
2. 新建您的分支

	- feature/[author]/[description]
	- hotfix/[author]/[isuse_id]
	
3. 提交代码
4. 新建 `Pull Request`
5. 坐等作者 `Merge`